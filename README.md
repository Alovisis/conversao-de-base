# Algoritmo para Conversão de Base - Decimal para Binário


## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11521036/avatar.png?width=90)  | Gabriel Alovisi  | Alovisis | [gabriel.alovisi1@gmail.com](mailto:gabriel.alovisi1@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11492692/avatar.png?width=90)  | Matheus Avila  | matheusavila | [matheusavila@alunos.utfpr.edu.br](mailto:matheusavila@alunos.utfpr.edu.br)




