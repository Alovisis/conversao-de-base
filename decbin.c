#include <stdio.h>

long int binario(const int val)
{
    return (val == 0)?0: binario(val/2)*10 + val%2;
}

int main()
{
    int decimal;
    printf("Digite um numero decimal: ");
    scanf("%d", &decimal);
    printf("%ld", binario(decimal));

    return 0;

}